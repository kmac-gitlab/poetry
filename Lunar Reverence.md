# Lunar Reverence

2023-07-12

She kneels in devoted adoration
Arms curved towards object of affection
Her attention directed completely
in worship of one Celesteial body.
Her soft eyes trace across the sky's spaces,
studies patiently the many faces,
the arcing throughout the lunar phases;
Learning to love the mood swings of moon beams
which illuminate the infinity.
"Come here, my dear, up to the stratosphere."
Entranced they dance, captured and enraptured,
floating and weightless, without gravity.
Until fire brings light, burns the night away;
At her end, dies to live another day.