# Gjeldsslave

Rage at the Kosm, my living like this
Pulled from the freedom of sweet nothingness
And forced to subsist! to work! to exist!
For what? Fleeting moments of happiness?

Subjugate yourself to the land's owner
(Sacrifice your time, your body, your will)
While he pays you a few measly kroner
For the house you've built on top of his hill

Honest trade for the fruits of your labour?
Free me from this seiðr that binds dear Völva
The borrower is slave to the lender
I refuse to remain a gjeldslave

Scarcely could I believe that we were born
Saddled like cattle for the few highborn.
