# Joy at the Man

The Man lifts its arms
Outlined in a victorial verdant
A celebration of life
An effigy of triumph
Look upon my Works, ye Mighty, and despair!
For but a moment Ozymandias stands tall and proud
The lone and level sands obscured by hallways in the round
Where once there was nothing
Where soon there will be nothing
A clock city now bustles, 
timeless, yet ephemeral
Filled with Waking Dreams
Aesthetically adorned by audacious artwork
Permeated by melodies demanding mortal flesh to dance
Creating connections through collisions of circumstance and chance
Fireworks light up the sky
The Man erupts in flames, arms raised in fiery defiance
As if to say, "Fuck your Burn."
"I was here. I am here. I will return."
Denying the hold of the abyss
Life is but a flicker
So we dance and sing and make art while it lasts
Cry tears of overwhelming joy at how beautiful it is   
Laugh in the face of sorrow, at the absurdity of it all
We love, we fall, we try again
And we remember who we are
As The Man becomes coal and ashes and smoke

