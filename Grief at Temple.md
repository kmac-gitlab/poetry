# Grief at Temple

Walk, do not run, to the Temple
Amble slowly towards your destination
as you set your intention
Cover your head when you enter
Linger your gaze on the photographs and letters to those we lost
Drag your fingertips along the grain of the wood and let it guide you toward the inner sanctum
There, sit or lie, on the dust
And think of me
Think of me softly
Remember me fondly
of the happy times
when I was well
and we were healthy
and we'd laugh or dance or sing
Know then in your heart
That I loved you
That I love you
That I will always love you
That I am love
That I will forever be the Parasol to your Archipelago
Then grieve me.
For your sake as well as mine.
Release me into the ether
Time passes, life moves on,
The sands shift, carried by the wind.
From ashes to ashes,
From stardust to stardust,
The Temple burns.